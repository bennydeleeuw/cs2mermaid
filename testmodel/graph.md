```mermaid
classDiagram
	class ClassX {
		+ Size: string
		+ Number: int
		+ InstanceOfClassY: ClassY
	}
	class ClassY {
		+ Last: string
	}
	class Main {
		+ Name: string
		+ Title: string
		+ ListOfClassX: ClassX[]
		+ ArrayClassX: ClassX[]
		+ InstanceOfClassY: ClassY
	}
ClassX -- ClassY
Main -- ClassX
Main -- ClassY
```
