namespace ProductDetail.Models;

public class Main
{

    public string Name { get; set; }
    public string Title { get; set; }

    public List<ClassX> ListOfClassX {get;set;}
    public ClassX[] ArrayClassX {get;set;}

    public ClassY InstanceOfClassY {get;set;}

}