FROM node:18-alpine

# Create app directory
WORKDIR /app

COPY parse.js .

CMD [ "node", "parse.js", "/app/models" ]