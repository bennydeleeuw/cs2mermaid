const fs = require('fs');
const path = require('path');

function readLinesFromFilesInDirectory(directoryPath) {
    try {
        const files = fs.readdirSync(directoryPath);



        files.forEach((file) => {


            const filePath = path.join(directoryPath, file);

            if (fs.statSync(filePath).isFile() && path.extname(filePath) === '.cs') {

                console.log(`Reading file: ${file}`);

                const fileContent = fs.readFileSync(filePath, 'utf8');
                const lines = fileContent.split('\n');

                for (let line of lines) {
                    // Process each line of the file
                    processLine(line);
                }
            }
        });

        console.log('All files processed successfully.');
    } catch (err) {
        console.error('Error reading directory:', err);
    }
}

var classStart = false;
var currentClass = "";
function checkClass(line) {

    var groups = line.match(/.*(public|private).*class\W*(\w*)/)
    if (!groups)
        return

    var [_, privpub, className] = groups
    classStart = true;
    currentClass = className


    return "\tclass " + className + " {\n";

}

function checkClassEnd(line) {

    if (!classStart)
        return

    var groups = line.match(/^\W*(})/)
    if (!groups)
        return

    classStart = false;
    currentClass = null
    return "\t}\n";

}

function checkProperty(line) {

    var groups = line.match(/.*(p.*\s\w*?)\W?([\w<>?\[\]]*)\W(\w*)\W{.*/)
    if (!groups)
        return

    var [_, privpub, type, name] = groups

    if (!privpub.startsWith("public"))
        return

    classRelations.push({classA: currentClass, classB: type})

    var typeOutput = type.replace(/^List<(.+)>\??$/, '$1[]').replace(/^List<(.+)$/, '$1');

    return `\t\t+ ${name}: ${typeOutput}\n`;

}
function processLine(line) {


    var newLine = checkClass(line)
    if (!newLine) newLine = checkProperty(line)
    if (!newLine) newLine = checkClassEnd(line)


    if (!newLine)
        return;

    fs.appendFileSync(filePath, newLine, 'utf8');
}


var args = process.argv.slice(2);

var filePath = args[0] + "/graph.md";
const directoryPath = args[0]; //'./src';

var classRelations = [];

fs.writeFileSync(filePath, "")
fs.appendFileSync(filePath, "```mermaid\n")
fs.appendFileSync(filePath, "classDiagram\n")

readLinesFromFilesInDirectory(directoryPath);

var realClasses = classRelations.map(x => x.classA);

var filteredRelations = classRelations.filter(x => {
    return realClasses.includes(x.classB) || realClasses.some(str => new RegExp('<(' + str + ')>').test(x.classB))
})

filteredRelations.forEach((relation) => {
    var classBGroups = relation.classB.match(/^(\w*)$|<(\w*)>/)
    var classB = classBGroups[1] ?? classBGroups[2]

    var str = `${relation.classA} -- ${classB}\n`
    fs.appendFileSync(filePath, str)
})

fs.appendFileSync(filePath, "```\n")
